package org.pickcellslab.pickcells.neodb;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.pickcells.neodb.adapters.NeoItemFactory;

public class AdaptedNodeEvaluator implements Evaluator {

	private final ExplicitPredicate<? super NodeItem> filter;
	private final Evaluation ifYes;
	private final NeoItemFactory f;
	private final Evaluation ifNot;

	public AdaptedNodeEvaluator(ExplicitPredicate<? super NodeItem> filter, Evaluation ifYes, Evaluation ifNot, NeoItemFactory f) {
		this.filter = filter;
		this.f = f;
		this.ifYes = ifYes;
		this.ifNot = ifNot;
	}

	@Override
	public Evaluation evaluate(final Path path) {
		Node n = path.endNode();
		NodeItem adapter = f.getNodeReadOnly(n);
		if(filter.test(adapter))
			return ifYes;
		else return ifNot;
	}


}