package org.pickcellslab.pickcells.neodb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.UniqueFactory;
import org.neo4j.graphdb.index.UniqueFactory.UniqueEntity;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.QInterpreter;
import org.pickcellslab.foundationj.dbm.events.DataUpdateListener;
import org.pickcellslab.foundationj.dbm.events.MetaListenerNotifier;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener.MetaChange;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems.RegeneratedItemsBuilder;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetaModelStorer implements QInterpreter<MetaBox> {

	private final GraphDatabaseService db;

	private final MetaListenerNotifier notifier;

	private final Logger log = LoggerFactory.getLogger(MetaModelStorer.class);


	private Map<WritableDataItem, Node> pairs = new LinkedHashMap<>();

	private Map<MetaModel,Node> temp = new HashMap<>(2);

	private final RegeneratedItemsBuilder changes = new RegeneratedItemsBuilder();

	private static final String METAMODEL = "MetaModel";
	private final Map<String, MetaNodeFactory> nFactories = new HashMap<>();


	static final String recString = MetaModel.recognition.toString();


	MetaModelStorer(GraphDatabaseService db, MetaListenerNotifier notifier){
		this.db = db;
		this.notifier = notifier;
	}



	// Used by StoreQInterpreter for storing the MetaModel elements which are created automatically
	// No need to synchronize here, caller is already (in fact synchronizing here causes deadlocks).
	void store(MetaInfo meta) throws DataAccessException{	
		//System.out.println("MetaModelStorer: Storing MetaInfo");
		for(StepTraverser<NodeItem,Link> t : meta.traversers()){
			storeTraversal(t);
		}
	}



	@Override
	public synchronized void run(MetaBox query) throws DataAccessException {
		//System.out.println("MetaModelStorer: Storing MetaBox");
		//Start the transaction
		try (Transaction tx = db.beginTx()) {
			storeTraversal(query.get());			
			tx.success();
			tx.close();
		}
		validateCommit();
	}





	GraphDatabaseService getDB(){
		return db;
	}


	private void storeTraversal(StepTraverser<NodeItem,Link> t) throws DataAccessException{

		log.trace("new meta graph to store");
		List<Link> linksToStore = new ArrayList<>();

		TraversalStep<NodeItem,Link> step = null;

		while ((step = t.nextStep()) != null) {

			for(WritableDataItem i : step.nodes){
				pairs.put(i, createIfNotExist((MetaModel) i));
			}

			for(Link l : linksToStore){
				createLinkIfNotExist(l);
			}
			linksToStore = step.edges;
		}

		for(Link l : linksToStore){
			createLinkIfNotExist(l);
		}		

		pairs.clear();
		temp.clear();

	}

	private Node createIfNotExist(MetaModel item) throws DataAccessException {

		// Here we need a different strategy than the one used in StorageBoxInterpreter
		// We need to look up the database for nodes with the same recognition string

		//Check if node exists and retrieve
		//Check class and get adequate label
		//System.out.println("MetaModelStorer : getOrCreate : "+item);
		//System.out.println("MetaModelStorer : getOrCreate : "+item.recognitionString());
		
		MetaNodeFactory uF = nFactories.get(item.typeId()); 
		if(null == uF){
			uF = new MetaNodeFactory(item);
			nFactories.put(item.typeId(), uF);
		}
		
		
		
		UniqueEntity<Node> node = uF.getOrCreateWithOutcome(recString, item.recognitionString());
		if(node.wasCreated()){
			changes.addAsTarget(item);
			final Node fnode = node.entity();
			item.getValidAttributeKeys().forEach(e->fnode.setProperty(e.toString(), item.getAttribute(e).get()));
		}
		else{
						
			try {
				
			// Watchdog code
			if(! item.recognitionString().equals(node.entity().getProperty(recString))) {
				System.out.println("PickCells Item of class : "+item.getClass());
				item.getValidAttributeKeys().forEach(k -> System.out.println("Key = "+k+ " Value = "+item.getAttribute(k)));
				System.out.println("========= Now listening properties Neo Node ======= ");
				node.entity().getAllProperties().forEach((k,v)->System.out.println("Key = "+k+ " Value = "+v));	
				throw new DataAccessException("Confused recognitions");
			}
			
			}catch(NotFoundException e) {	
				System.out.println("PickCells Item of class : "+item.getClass());
				item.getValidAttributeKeys().forEach(k -> System.out.println("Key = "+k+ " Value = "+item.getAttribute(k)));
				System.out.println("========= Now listening properties Neo Node ======= ");
				node.entity().getAllProperties().forEach((k,v)->System.out.println("Key = "+k+ " Value = "+v));				
				throw new DataAccessException("Inconsistent Node State (No recognition string)");
			}
			
		}
		
				
		return node.entity();

	}


	private void createLinkIfNotExist(Link l) {

		// Again we need a different strategy than the one used in StorageBoxInterpreter
		// We need to look up the nodes to see if the relationship already exists
		Relationship relationship = null;
		setSourceAndTarget(l);

		//Get the relationships from the node that is not a metaclass (less relationships)
		Iterable<Relationship> rels = null;
		if(!MetaClass.class.isAssignableFrom(l.source().getClass())){

			//log.trace("l.source is null "+(l.source() == null));
			//log.trace("temp.getlsource is null "+(temp.get(l.source()) == null));
			rels = temp.get(l.source()).getRelationships();

		}
		else//use the target
			rels = temp.get(l.target()).getRelationships();

		for(Relationship r : rels){
			//check type
			if(!r.getType().name().equals(l.declaredType()))
				continue;
			//check if source and target point to the same equivalent item (MetaItem/Node)       
			boolean equalSource = r.getStartNode().equals(temp.get(l.source()));
			boolean equalTarget = r.getEndNode().equals(temp.get(l.target()));
			if(equalSource && equalTarget){
				relationship = r;
				break;
			}
		}

		//Create the relationship if it is null
		if(relationship == null){
			relationship = temp.get(l.source()).createRelationshipTo(temp.get(l.target()),
					RelationshipType.withName(l.declaredType()));		
		}
		

		//Update the keys
		final Relationship frelationship = relationship;
		l.getValidAttributeKeys().forEach(k->frelationship.setProperty(k.toString(), l.getAttribute(k).get()));

	}


	private void setSourceAndTarget(Link l){
		// get Source node and target node
		temp.clear();
		//System.out.println("MetaModelStorer : Storing : "+l);
		//System.out.println("MetaModelStorer : Source mapping : "+pairs.get(l.source()).getProperty(recString));
		//System.out.println("MetaModelStorer : Target mapping : "+pairs.get(l.target()).getProperty(recString));
		temp.put((MetaModel) l.source(), pairs.get(l.source()));
		temp.put((MetaModel) l.target(), pairs.get(l.target()));
	}



	/**
	 * Called by StorageBoxInterpreter when the transaction was committed to allow for firing events
	 */
	void validateCommit(){
		notifier.notify(changes.build(), MetaChange.CREATED);
		changes.reset();
	}








	@Override
	public void setUpdateListenerList(List<DataUpdateListener> lstr) {
		// Does not make any update

	}



	private class MetaNodeFactory extends UniqueFactory.UniqueNodeFactory{

		
		private final Label typeIdValue;
		private final Set<Label> tags;


		MetaNodeFactory( MetaModel item){
			super(db.index().forNodes(METAMODEL));
			typeIdValue = Label.label(item.typeId());

			log.trace("New factory for MetaType : "+typeIdValue);

			tags = Helpers.getTags(item.getClass()).keySet();

		};			


		@Override
		protected void initialize( Node created, Map<String, Object> properties )
		{			
			//created.setProperty( recString, properties.get( recString ) );
			created.setProperty(Helpers.typeIdKey, typeIdValue.name());
			System.out.println("New MetaItem : "+properties.get( recString ));
			created.addLabel(typeIdValue);
			for(Label l : tags)
				created.addLabel(l);
					
		}

	}

	

}
