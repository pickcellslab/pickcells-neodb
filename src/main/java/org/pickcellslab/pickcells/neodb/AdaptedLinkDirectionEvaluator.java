package org.pickcellslab.pickcells.neodb;

import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.pickcellslab.foundationj.datamodel.Direction;

public class AdaptedLinkDirectionEvaluator implements Evaluator {

	private final Direction dir;
	private final RelationshipType type;

	public AdaptedLinkDirectionEvaluator(RelationshipType type, Direction dir) {
		this.dir = dir;
		this.type = type;
	}


	@Override
	public Evaluation evaluate(Path path) {
		Relationship r = path.lastRelationship();
		if(r==null)
			return Evaluation.INCLUDE_AND_CONTINUE;
		
		Evaluation eval = Evaluation.INCLUDE_AND_CONTINUE;
		
		//Checking the type
		if(r.isType(type)){
			
			//Checking the direction
			
			switch(dir){
			case BOTH: eval = Evaluation.EXCLUDE_AND_PRUNE;
				break;
			case INCOMING: eval = r.getEndNode().equals(path.endNode()) ? Evaluation.INCLUDE_AND_CONTINUE : Evaluation.EXCLUDE_AND_PRUNE;
				break;
			case OUTGOING: eval = r.getEndNode().equals(path.endNode()) ?  Evaluation.EXCLUDE_AND_PRUNE : Evaluation.INCLUDE_AND_CONTINUE;
				break;			
			}
			
		}
		
		return eval;
	}

}
