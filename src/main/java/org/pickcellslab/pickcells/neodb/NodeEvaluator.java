package org.pickcellslab.pickcells.neodb;

import java.util.function.Predicate;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.pickcellslab.foundationj.datamodel.tools.Decision;

public class NodeEvaluator implements Evaluator{

	private final Predicate<Node> filter;
	private final Evaluation eval;

	NodeEvaluator(Predicate<Node> filter, Decision d) {
		this.filter = filter;
		switch(d){
		case EXCLUDE_AND_CONTINUE: eval = Evaluation.EXCLUDE_AND_CONTINUE;
		break;
		case EXCLUDE_AND_STOP: eval = Evaluation.EXCLUDE_AND_PRUNE;
		break;
		case INCLUDE_AND_CONTINUE: eval = Evaluation.INCLUDE_AND_CONTINUE;
		break;
		case INCLUDE_AND_STOP: eval = Evaluation.INCLUDE_AND_PRUNE;
		break;
		default: eval = null;
		break;			
		}

	}

	@Override
	public Evaluation evaluate(final Path path) {
		Node n = path.endNode();
		if(filter.test(n))
			return eval;
		else return Evaluation.EXCLUDE_AND_CONTINUE;
	}

}
