package org.pickcellslab.pickcells.neodb;

import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;

public class AdaptedEvaluators implements Evaluator {

	private Evaluator[] evals;
	private Evaluation df;

	public AdaptedEvaluators(Evaluator[] evals, Evaluation df) {
		this.evals = evals;
		this.df = df;
	}

	@Override
	public Evaluation evaluate(final Path path) {
		for ( Evaluator evaluator : evals )
        {
			Evaluation e = evaluator.evaluate( path );
            if ( e != df )
            {
                return e;
            }
        }
        return df;
	}


}
