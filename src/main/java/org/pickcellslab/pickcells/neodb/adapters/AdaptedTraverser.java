package org.pickcellslab.pickcells.neodb.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.LinkEvaluation;
import org.pickcellslab.foundationj.datamodel.tools.MorphedIterator;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.PathTraverser;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints.LinksLogic;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.pickcells.neodb.AdaptedEvaluators;
import org.pickcellslab.pickcells.neodb.AdaptedLinkDirectionEvaluator;
import org.pickcellslab.pickcells.neodb.AdaptedLinkEvaluator;
import org.pickcellslab.pickcells.neodb.AdaptedNodeEvaluator;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;

public class AdaptedTraverser implements PathTraverser<Node,Relationship> {

	private final Traverser tr;
	private final NeoItemFactory f;

	public AdaptedTraverser(GraphDatabaseService db, TraversalDefinition description, NeoItemFactory f){

		this.f = f;

		//Setting up the Neo4j TraversalDescription as specified by the query

		//Find the startNode, return null

		Label startNodeLabel = Label.label(description.getStartNodeClass()); 

		Iterable<Node> roots = null;

		if(description.usesIndexedProperty())
			roots = Helpers.asIterable(
					db.findNodes(startNodeLabel, description.getStartKey().toString(), 
							description.getStartValue())
					);
		else{
			Iterable<Node> toFilter = Helpers.asIterable(db.findNodes(startNodeLabel));
			//We now need to filter roots
			ExplicitPredicate<? super NodeItem> filter = description.rootPredicate();
			if(null == filter)
				roots = toFilter;
			else{
				roots = new ArrayList<>();				
				for(Node n : toFilter){
					NodeItem adapter = f.getNodeReadOnly(n);
					if(filter.test(adapter))
						((ArrayList<Node>) roots).add(n);
				}
			}
		}


		if(!roots.iterator().hasNext()){
			tr = null;
			return;
		}


		//Depth
		org.neo4j.graphdb.traversal.TraversalDescription td = db.traversalDescription() 
				.breadthFirst()
				.evaluator(Evaluators.fromDepth(description.minDepth()))
				.evaluator(Evaluators.toDepth(description.maxDepth())
						);

		//Uniqueness
		switch(description.traversalPolicy()){// when depth is the only stop condition
		case LINKS_ONCE:	td = td.uniqueness(Uniqueness.RELATIONSHIP_GLOBAL);
			break;
		case NODES_ONCE:	td = td.uniqueness(Uniqueness.NODE_GLOBAL);
			break;
		case NONE:			td = td.uniqueness(Uniqueness.NONE);
			break;		
		}
	

		//Node filters and decision when the test is true
		if(!description.allNodesIncluded()){
				
			Evaluation df = Helpers.getNeo4jDecision(description.defaultDecision());
			
			//if(description.defaultDecision() == Decision.EXCLUDE_AND_CONTINUE || description.defaultDecision() == Decision.EXCLUDE_AND_STOP ){

				Evaluator[] evals = new Evaluator[description.getEvaluations().size()];

				int i = 0;
				for(Entry<ExplicitPredicate<? super NodeItem>, Decision> e : description.getEvaluations().entrySet())
					evals[i++] =
					new AdaptedNodeEvaluator(e.getKey(),
							Helpers.getNeo4jDecision(e.getValue()),
							df,
							f);
				td = td.evaluator(new AdaptedEvaluators(evals, df));

			}
		/*	
			else
			
				for(Entry<ExplicitPredicate<? super NodeItem>, Decision> e : description.getEvaluations().entrySet())
					td = td.evaluator(
							new AdaptedNodeEvaluator(e.getKey(),
									Helpers.getNeo4jDecision(e.getValue()),
									Helpers.getNeo4jDecision(description.defaultDecision()),
									f));

		}
		*/


		//Relationships filters
		if(!description.traverseAllLinks()){
			if(description.logicOnLinks() == LinksLogic.INCLUSION)
				for(Entry<String, Direction> i : description.getLinkConstraints().entrySet())
					td = td.relationships(RelationshipType.withName(i.getKey()), Helpers.getNeo4jDirection(i.getValue()));
			else
				for(Entry<String, Direction> i : description.getLinkConstraints().entrySet())
					td = td.evaluator(new AdaptedLinkDirectionEvaluator(RelationshipType.withName(i.getKey()),i.getValue()));
		}

		
		
		
		LinkEvaluation lastLink = description.getLinkTargetEvaluation();
		if(description.getLinkTargetEvaluation()!=null){
			RelationshipType t = RelationshipType.withName(lastLink.evaluatedType());
			ExplicitPredicate<? super Link> p = lastLink.predicate();
			Evaluation ifYes = Helpers.getNeo4jDecision(lastLink.ifYes());
			Evaluation ifNot = Helpers.getNeo4jDecision(lastLink.ifNo());
			td = td.evaluator(new AdaptedLinkEvaluator(t,p,ifYes,ifNot, Helpers.getNeo4jDecision(description.defaultDecision()), f));
		}
		

		tr = td.traverse(roots);
	}






	@Override
	public Traversal<Node,Relationship> traverse() {


		if(tr!=null)
			return new Traversal<Node,Relationship>(){
			@Override
			public Iterable<Node> nodes() {
				return tr.nodes();
			}
			@Override
			public Iterable<Relationship> edges() {
				return tr.relationships();
			}
		};


		else
			return new Traversal<Node,Relationship>(){
			@Override
			public Iterable<Node> nodes() {
				return Collections.emptyList();
			}
			@Override
			public Iterable<Relationship> edges() {
				return Collections.emptyList();
			}
		};

	}






	@Override
	public Iterator<Path<Node, Relationship>> paths() {
		return new MorphedIterator<org.neo4j.graphdb.Path,Path<Node, Relationship>>(
				tr.iterator(), (p)->f.path(p));

	}
	
	
	
	
	
	

}
