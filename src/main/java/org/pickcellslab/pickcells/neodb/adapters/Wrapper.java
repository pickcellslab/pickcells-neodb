package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.PropertyContainer;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.mapping.data.DbElement;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;

public class Wrapper implements NodeItem, DbElement{

	
	private PropertyContainer container;

	
	public void wrap(PropertyContainer container){
		this.container = container;
	}
	
	
	@Override
	public boolean isTagged(String tag) {
		return ((Node)container).hasLabel(Label.label(tag));
	}
	
	public boolean hasTag(String tag) {
		return isTagged(tag);
	}
	
	@Override
	public final Stream<AKey<?>> getValidAttributeKeys() {
		return StreamSupport.stream(container.getPropertyKeys().spliterator(), false).filter(s->!s.equals(Helpers.typeIdKey)).map(s->AKey.get(s));
	}

	@Override
	public final <T> void setAttribute(AKey<T> key, T v) {
		throw new RuntimeException(" This object is read-only ");
	}

	@Override
	public final void removeAttribute(AKey<?> key) {
		throw new RuntimeException(" This object is read-only ");  
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public final <T> Optional<T> getAttribute(AKey<T> key) {
		return Optional.ofNullable((T) container.getProperty(key.toString(),null));
	}


	@Override
	public Stream<AKey<?>> minimal() {
		throw new RuntimeException("Not implemented");
	}




	@Override
	public Stream<Link> links() {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public String declaredType() {
		return (String)container.getProperty(DataNode.declaredTypeKey.toString(), typeId());
	}
	
	
	@Override
	public String typeId() {
		return (String)container.getProperty(Helpers.typeIdKey);
	}


	@Override
	public int getDegree(Direction direction, String linkType) {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public Stream<Link> getLinks(Direction direction, String... linkTypes) {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public boolean addLink(Link link) {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public boolean removeLink(Link link, boolean updateNeighbour) {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public Collection<Link> removeLink(Predicate<Link> predicate, boolean updateNeighbour) {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public Link addOutgoing(String type, NodeItem target) {
		throw new RuntimeException("Not implemented");
	}


	@Override
	public Link addIncoming(String type, NodeItem target) {
		throw new RuntimeException("Not implemented");
	}


	
}
