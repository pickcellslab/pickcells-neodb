package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.PropertyContainer;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.db.DatabaseNode;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.db.UpdateInfo;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;

public class NeoUpdatableNode extends UpdatableNode implements DatabaseNode {


	protected final PropertyContainer container;
	protected final NeoItemFactory f;
	
	public NeoUpdatableNode(NeoItemFactory fj, PropertyContainer v, UpdateInfo ui) {
		super(ui);
		this.f = fj;
		this.container = v;
	}
	
	
	@Override
	public Stream<AKey<?>> getValidAttributeKeys() {
		return StreamSupport.stream(container.getPropertyKeys().spliterator(), false).filter(s->!s.equals(Helpers.typeIdKey)).map(s->AKey.get(s));
	}

	@Override
	protected<T> void set(AKey<T> key, T v) {
		container.setProperty(key.toString(), v);
	}

	@Override
	protected <T> void remove(AKey<T> key) {
		container.removeProperty(key.toString());
	}


	@SuppressWarnings({ "unchecked" })
	@Override
	public <T> Optional<T> getAttribute(AKey<T> key) {
		return Optional.ofNullable((T) container.getProperty(key.toString(),null));
	}
	
	
	
	
	@Override
	public boolean hasTag(String tag) {
		return ((Node) container).hasLabel(Label.label(tag));
	}
	

	@Override
	public String declaredType() {
		return (String)container.getProperty(DataNode.declaredTypeKey.toString(), typeId());
	}
	
	
	@Override
	public String typeId() {
		return (String)container.getProperty(Helpers.typeIdKey);
	}
	
	
	@Override
	public Iterator<String> getTags() {
		return Helpers.getTags((Node) container);
	}

	@Override
	public Stream<Link> getLinks(Direction direction, String... linkTypes) {

		Iterable<Relationship> rels = null;
		RelationshipType[] relTypes = null;
		Node node = (Node) container;

		if(linkTypes.length != 0){
			relTypes = new RelationshipType[linkTypes.length];
			for(int i = 0; i<linkTypes.length; i++)
				relTypes[i] = RelationshipType.withName(linkTypes[i]);


			//System.out.println("relTypes is null = "+(relTypes == null));

			switch(direction){
			case BOTH : rels = node.getRelationships(org.neo4j.graphdb.Direction.BOTH, relTypes);
			break;
			case INCOMING : rels = node.getRelationships(org.neo4j.graphdb.Direction.INCOMING, relTypes);
			break;
			case OUTGOING : rels = node.getRelationships(org.neo4j.graphdb.Direction.OUTGOING, relTypes);
			break;      
			}

		}else{
			switch(direction){
			case BOTH : rels = node.getRelationships(org.neo4j.graphdb.Direction.BOTH);
			break;
			case INCOMING : rels = node.getRelationships(org.neo4j.graphdb.Direction.INCOMING);
			break;
			case OUTGOING : rels = node.getRelationships(org.neo4j.graphdb.Direction.OUTGOING);
			break; 
			}
		}

		if(rels != null)
			return StreamSupport.stream(rels.spliterator(), false).map(r->f.getUpdatableLink(r, ui));
			
		else
			return Stream.empty();
	}

	@Override
	public Stream<Link> links() {
		final Node node = (Node) container;
		return StreamSupport.stream(node.getRelationships().spliterator(), false).map(r->f.getUpdatableLink(r,ui));
	}


	@Override
	public boolean addLink(Link link) {
		throw new RuntimeException("This Node is Read-Only");
	}

	@Override
	public Link addOutgoing(String type, NodeItem target) {
		throw new RuntimeException("This Node is Read-Only");
	}

	@Override
	public Link addIncoming(String type, NodeItem target) {
		throw new RuntimeException("This Node is Read-Only");
	}

	

	@Override
	public boolean removeLink(Link link, boolean updateNeighbour) {
		throw new RuntimeException("This Node is Read-Only");
	}

	@Override
	public Collection<Link> removeLink(Predicate<Link> predicate,
			boolean updateNeighbour) {
		throw new RuntimeException("This Node is Read-Only");
	}

	
	@Override
	public boolean equals(Object o){
		if(!NeoUpdatableNode.class.isAssignableFrom(o.getClass())) return false;
		if(((NeoUpdatableNode)o).container.equals(this.container)) return true;		
		return false;
	}

	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.empty();
	}
	
	
	@Override
	public String toString(){
		return declaredType()+" "+ container.getProperty(WritableDataItem.idKey.toString());
	}

	@Override
	public int getDegree(Direction direction, String linkType) {
		return	((Node)container).getDegree(RelationshipType.withName(linkType), Helpers.getNeo4jDirection(direction));
	}


	

	
	
}
