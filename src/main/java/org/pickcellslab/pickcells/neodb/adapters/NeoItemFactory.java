package org.pickcellslab.pickcells.neodb.adapters;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.db.DatabaseNode;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.db.UpdatableLink;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.db.UpdateInfo;

/**
 * A Factory class to obtain objects which adapt the Neo4j interfaces to the foundationJ interfaces
 * The objects can be obtained as prototypes where one foundationJ instance is used to host multiple
 * neo4j instances (used if access is sequential) or as real adapter where one neo4j instance corresponds 
 * to one and only one final neo4j instance.
 * 
 * @author Guillaume Blin
 *
 */
public class NeoItemFactory implements FjAdaptersFactory<Node,Relationship> {

	final GraphDatabaseService db;

	private final Map<Node, DatabaseNode> items = new HashMap<>();
	private final Map<Relationship, Link> links = new HashMap<>();
	private final Map<Node, NeoItemDeletable> itemsDeletable = new HashMap<>();
	private final Map<Node, UpdatableNode> updatableNodes = new HashMap<>();
	private final Map<Relationship, UpdatableLink> updatableLinks = new HashMap<>();
	private final Map<Relationship, NeoLinkDeletable> linksDeletable = new HashMap<>();
	private final Map<org.neo4j.graphdb.Path, Path<Node,Relationship>> paths = new HashMap<>();

	public NeoItemFactory(GraphDatabaseService db) {
		this.db = db;
	}


	@Override
	public DatabaseNode getNodeReadOnly(Node n) {
		synchronized(items) {
			DatabaseNode l = items.get(n);
			if(null==l){
				l = new NeoItemReadOnly(this,n);
				items.put(n,l);
			}	
			return l;
		}
	}

	@Override
	public DatabaseNodeDeletable getNodeDeletable(Node n) {
		synchronized(itemsDeletable) {
			NeoItemDeletable l = itemsDeletable.get(n);
			if(null==l){
				l = new NeoItemDeletable(this,n);
				items.put(n,l);
			}			
			return l;
		}
	}



	@Override
	public UpdatableNode getUpdatableNode(Node v, UpdateInfo ui) {
		synchronized(updatableNodes) {
			UpdatableNode l = updatableNodes.get(v);
			if(null==l){
				l = new NeoUpdatableNode(this,v, ui);
				updatableNodes.put(v,l);
			}			
			return l;
		}
	}




	@Override
	public Link getLinkReadOnly(Relationship r) {
		synchronized(links) {
			Link l = links.get(r);
			if(null==l && null != r){
				l = new NeoLinkReadOnly(this, r);
				links.put(r,l);
			}			
			return l;
		}
	}

	@Override
	public Link getLinkDeletable(Relationship r) {
		synchronized(linksDeletable) {
			NeoLinkDeletable l = linksDeletable.get(r);
			if(null==l){
				l = new NeoLinkDeletable(this, r);
				links.put(r,l);
			}			
			return l;
		}
	}



	@Override
	public UpdatableLink getUpdatableLink(Relationship v, UpdateInfo ui) {
		synchronized(updatableLinks) {
			UpdatableLink l = updatableLinks.get(v);
			if(null==l){
				l = new NeoUpdatableLink(this,v, ui);
				updatableLinks.put(v,l);
			}			
			return l;
		}
	}



	void remove(NeoItemDeletable i) {
		itemsDeletable.remove(i.container);
	}

	public Path<Node,Relationship> path(org.neo4j.graphdb.Path p) {
		synchronized(paths) {
			Path<Node,Relationship> path = paths.get(p);
			if(null == path){
				path = new NeoAdaptedPath(p);
				paths.put(p, path);
			}
			return path;
		}
	}








}




