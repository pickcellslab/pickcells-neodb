package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Objects;

import org.neo4j.graphdb.Relationship;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Deletable;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;

public class NeoLinkReadOnly extends NeoDataReadOnly implements Link, Deletable{

	private final NodeItem source;
	private final NodeItem target;


	NeoLinkReadOnly(NeoItemFactory f, Relationship relation) {		
		super(f,relation);  	
		Objects.requireNonNull(relation);
		this.source = f.getNodeReadOnly(relation.getStartNode());
		this.target = f.getNodeReadOnly(relation.getEndNode());   	
	}

	@Override
	public String declaredType() {
		return ((Relationship) container).getType().name();
	}

	@Override
	public NodeItem source() {
		return source ;
	}

	@Override
	public NodeItem target() {
		return target;
	}

	@Override
	public void delete() {
		throw new RuntimeException(" Deletion is not permitted ");
	}

	public void delete(AKey<?> k){
		throw new RuntimeException(" This Link is read-only ");
	}


	@Override
	public boolean equals(Object o){
		if(!getClass().isAssignableFrom(o.getClass()))	return false;
		if(((NeoLinkReadOnly)o).container.equals(this.container)) return true;		
		return false;
	}

	
	@Override
	public String toString(){
		return "Link "+declaredType() + container.getProperty(DataItem.idKey.toString());
	}

}
