package org.pickcellslab.pickcells.neodb.adapters;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.StreamSupport;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule.Policy;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems.RegeneratedItemsBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.DataTypeException;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.pickcells.neodb.AdaptedEvaluators;
import org.pickcellslab.pickcells.neodb.AdaptedLinkDirectionEvaluator;
import org.pickcellslab.pickcells.neodb.AdaptedNodeEvaluator;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reconstructor {

	private static final Logger log = LoggerFactory.getLogger(Reconstructor.class);


	private static final String METAMODEL = "MetaModel";
	static final String recString = MetaModel.recognition.toString();
	
	private final GraphDatabaseService db;
	private final NeoItemFactory f;
	private final DataRegistry registry;

	public Reconstructor(GraphDatabaseService db, NeoItemFactory f, DataRegistry registry){
		this.db = db;
		this.f = f;
		this.registry = registry;
	}




	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RegeneratedItems retrieve(List<Node> nodes, ReconstructionRule rule, boolean delete) throws DataAccessException {

		//FIXME If nodes that did not pass the filter previously are
		//on a reconstruction path, then they will be retrieved
		//Is this a satisfactory behaviour?


		//Keep in cache the node to turn into items

		//TODO Think about replacing these maps with WeakHashMap to prevent multiple instances of the same node to hang around
		final Map<Node, NodeItem> items = new HashMap<>(nodes.size());
		final Map<Node, NodeItem> attached = new HashMap<>(nodes.size());

		final Wrapper wrapper = new Wrapper();

		//First reconstruct roots
		for(Node node : nodes){



			//Instantiate
			NodeItem ni;
			try {
				wrapper.wrap(node);
				ni = (NodeItem) registry.getDataInstance(wrapper);
			} catch (DataTypeException | InstantiationHookException e) {				
				throw new DataAccessException(e.getMessage());
			}


			//log.trace("Current item is a "+ni.getClass().getSimpleName());

			//Add properties
			rule.getKeysToRetrieve(ni, StreamSupport.stream(node.getPropertyKeys().spliterator(), false)
					.filter(s->!s.equals(Helpers.typeIdKey)))
			.forEach(s-> ni.setAttribute((AKey)AKey.get(s), node.getProperty(s)));
			// FIXME we potentially create a lot of AKey objects here use a Multiton approach


			//log.debug("Adding node : "+node.getLabels().iterator().next().toString());
			//log.debug("Adding item : "+ni.getClass().getSimpleName());
			items.put(node,ni);
		}






		//Setup the traversaldescription to go through the attached nodes

		//Depth
		TraversalDescription td = db.traversalDescription() 
				.uniqueness(Uniqueness.RELATIONSHIP_GLOBAL)
				.depthFirst()				
				.evaluator(Evaluators.toDepth(rule.getMaxDepth()))
				.evaluator(Evaluators.excludeStartPosition());


		//Define the Node Evaluators
		if(!rule.noNodeConstraints()){
			/*
			for(Entry<ExplicitPredicate<? super NodeItem>, Decision> e : rule.getDecisionsOnNodes().entrySet())
				td = td.evaluator(
						new AdaptedNodeEvaluator(e.getKey(),
								Helpers.getNeo4jDecision(e.getValue()),
								Helpers.getNeo4jDecision(rule.defaultDecision()),
								f));
			 */
			Evaluation df = Helpers.getNeo4jDecision(rule.defaultDecision());

			//if(description.defaultDecision() == Decision.EXCLUDE_AND_CONTINUE || description.defaultDecision() == Decision.EXCLUDE_AND_STOP ){

			Evaluator[] evals = new Evaluator[rule.getDecisionsOnNodes().size()];

			int i = 0;
			for(Entry<ExplicitPredicate<? super NodeItem>, Decision> e : rule.getDecisionsOnNodes().entrySet())
				evals[i++] =
				new AdaptedNodeEvaluator(e.getKey(),
						Helpers.getNeo4jDecision(e.getValue()),
						df,
						f);
			td = td.evaluator(new AdaptedEvaluators(evals, df));

		}



		//Define the Relationship Evaluators
		if(!rule.noLinkConstraints()){

			log.trace("Constraints on links detected");

			if(rule.linkPolicy() == Policy.INCLUDED)
				for(Entry<String, Direction> i : rule.getDecisionsOnLinks().entrySet())
					td = td.relationships(RelationshipType.withName(i.getKey()), Helpers.getNeo4jDirection(i.getValue()));
			else
				for(Entry<String, Direction> e : rule.getDecisionsOnLinks().entrySet())
					td = td.evaluator(new AdaptedLinkDirectionEvaluator(RelationshipType.withName(e.getKey()), e.getValue()));
		}







		//Traverse with each filtered node as a root 
		Traverser traverser = td.traverse(nodes.toArray(new Node[nodes.size()]));
		//Traverser traverser = td.traverse(filtered.get(0));


		LongAdder counter = new LongAdder();

		for(Node node : traverser.nodes()){			

			if(!items.containsKey(node)){//important!

				counter.increment();


				//Instantiate
				try {

					wrapper.wrap(node);					
					final NodeItem i = (NodeItem) registry.getDataInstance(wrapper);
					log.trace("Current item is a "+i.getClass().getSimpleName());

					//Add properties
					rule.getKeysToRetrieve(i, StreamSupport.stream(node.getPropertyKeys().spliterator(), false)
							.filter(s->!s.equals(Helpers.typeIdKey)))
					.forEach(s-> i.setAttribute((AKey)AKey.get(s), node.getProperty(s)));

					log.trace("Adding node : "+node.getLabels().iterator().next().toString());
					log.trace("Adding item : "+i.getClass().getSimpleName());
					attached.put(node,i);

				} catch (DataTypeException | InstantiationHookException e1) {
					throw new DataAccessException(wrapper.typeId() + "not supported",e1);
				}

			}

		}

		log.trace("Number of traversed nodes = "+counter);

		//Now that all the nodes were transformed into DataItem, make the Links
		counter.reset();
		for(Relationship r : traverser.relationships()){


			NodeItem start = items.get(r.getStartNode());
			if(start == null)
				start = attached.get(r.getStartNode());

			NodeItem end = items.get(r.getEndNode());
			if(end == null)
				end = attached.get(r.getEndNode());

			if(start != null && end != null){
				counter.increment();
				Link l = new DataLink(r.getType().name(), start, end, true);
				//Adding properties
				for(String s : r.getPropertyKeys()){
					l.setAttribute((AKey)AKey.get(s), r.getProperty(s));
				}
			}else{
				log.error("Null start or end node!");
			}
		}
		log.debug("Number of traversed relationships = "+counter);

		final RegeneratedItemsBuilder builder = new RegeneratedItemsBuilder();

		//set targets
		if(delete)
			for(Node t : new HashSet<>(nodes)){
				final NodeItem item = items.get(t);
				builder.addAsTarget(item);
				t.getRelationships().forEach(r->r.delete());
				if(item instanceof MetaModel){
					db.index().forNodes(METAMODEL).remove(t);
					log.debug(item+" removed from db index in principle");
				}
				t.delete();// Check index
			}
		else
			for(WritableDataItem t : items.values())
				builder.addAsTarget(t);

		//set dependencies
		for(WritableDataItem i : attached.values())
			builder.addTargetDependency(i);

		return builder.build();

	}







}
