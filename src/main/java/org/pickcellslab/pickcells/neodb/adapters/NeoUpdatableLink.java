package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.neo4j.graphdb.Relationship;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dbm.db.UpdatableLink;
import org.pickcellslab.foundationj.dbm.db.UpdateInfo;

public class NeoUpdatableLink extends UpdatableLink {

	protected final Relationship container;
	protected final NeoItemFactory f;
	private final NodeItem source;
	private final NodeItem target;
	
	public NeoUpdatableLink(NeoItemFactory fj, Relationship relation, UpdateInfo ui) {
		super(ui);
		this.f = fj;
		this.container = relation;
		Objects.requireNonNull(relation);
		this.source = f.getUpdatableNode(relation.getStartNode(), ui);
		this.target = f.getUpdatableNode(relation.getEndNode(), ui);
	}

	

	
	@Override
	public Stream<AKey<?>> getValidAttributeKeys() {
		return StreamSupport.stream(container.getPropertyKeys().spliterator(), false).map(s->AKey.get(s));
	}

	@Override
	protected<T> void set(AKey<T> key, T v) {
		container.setProperty(key.toString(), v);
	}




	@SuppressWarnings({ "unchecked" })
	@Override
	public <T> Optional<T> getAttribute(AKey<T> key) {
		return Optional.ofNullable((T) container.getProperty(key.toString(),null));
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.empty();
	}


	@Override
	public String declaredType() {
		return container.getType().name();
	}

	@Override
	public NodeItem source() {
		return source ;
	}

	@Override
	public NodeItem target() {
		return target;
	}

	@Override
	public void delete() {
		throw new RuntimeException(" Deletion is not permitted ");
	}

	public void delete(AKey<?> k){
		throw new RuntimeException(" This Link is read-only ");
	}


	@Override
	public boolean equals(Object o){
		if(!getClass().isAssignableFrom(o.getClass()))	return false;
		if(((NeoLinkDeletable)o).container.equals(this.container)) return true;		
		return false;
	}






	@Override
	protected <T> void remove(AKey<T> key) {
		// TODO Auto-generated method stub
		
	}

}
