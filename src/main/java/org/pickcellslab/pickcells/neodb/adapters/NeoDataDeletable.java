package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.neo4j.graphdb.PropertyContainer;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Deletable;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;

public abstract class NeoDataDeletable implements WritableDataItem , Deletable{

	protected final PropertyContainer container;
	protected final NeoItemFactory f;


	NeoDataDeletable(NeoItemFactory f, PropertyContainer container){
		this.f = f;
		this.container = container;
	}


	@Override
	public Stream<AKey<?>> getValidAttributeKeys() {		
		return StreamSupport.stream(container.getPropertyKeys().spliterator(), false).filter(s->!s.equals(Helpers.typeIdKey)).map(s->AKey.get(s));
	}

	@Override
	public <T> void setAttribute(AKey<T> key, T v) {
		container.setProperty(key.toString(), v);
	}

	
	@Override
	public void removeAttribute(AKey<?> key) {
		container.removeProperty(key.toString());    
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public <T> Optional<T> getAttribute(AKey<T> key) {
		return Optional.ofNullable((T) container.getProperty(key.toString(),null));
	}


}
