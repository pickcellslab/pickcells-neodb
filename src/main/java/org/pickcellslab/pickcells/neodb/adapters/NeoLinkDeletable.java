package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Objects;

import org.neo4j.graphdb.Relationship;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;

public class NeoLinkDeletable extends NeoDataDeletable implements Link{

	private final NodeItem source;
	private final NodeItem target;

	
	NeoLinkDeletable(NeoItemFactory f, Relationship relation) {		
		super(f,relation);  	
		Objects.requireNonNull(relation);
		this.source = f.getNodeReadOnly(relation.getStartNode());
		this.target = f.getNodeReadOnly(relation.getEndNode());   	
	}

	@Override
	public String declaredType() {
		return ((Relationship) container).getType().name();
	}

	@Override
	public NodeItem source() {
		return source ;
	}

	@Override
	public NodeItem target() {
		return target;
	}

	@Override
	public void delete() {
		((Relationship)container).delete();
	}

	void delete(AKey<?> k){
		((Relationship)container).removeProperty(k.toString());
	}
	
	
	@Override
	public boolean equals(Object o){
		if(!getClass().isAssignableFrom(o.getClass()))	return false;
		if(((NeoLinkDeletable)o).container.equals(this.container)) return true;		
		return false;
	}

	@Override
	public String toString() {
		return "db link : " + source + " -"+declaredType()+"-> "+target;
	}


}
