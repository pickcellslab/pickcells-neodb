package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.neo4j.graphdb.PropertyContainer;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class NeoDataReadOnly implements WritableDataItem{

	protected Logger log = LoggerFactory.getLogger(NeoDataReadOnly.class);

	protected final PropertyContainer container;
	protected final NeoItemFactory f;


	NeoDataReadOnly(NeoItemFactory f, PropertyContainer container){
		this.f = f;
		this.container = container;
	}


	@Override
	public final Stream<AKey<?>> getValidAttributeKeys() {
		return StreamSupport.stream(container.getPropertyKeys().spliterator(), false).filter(s->!s.equals(Helpers.typeIdKey)).map(s->AKey.get(s));
	}

	@Override
	public final <T> void setAttribute(AKey<T> key, T v) {
		throw new RuntimeException(" This Link is read-only ");
	}

	@Override
	public final void removeAttribute(AKey<?> key) {
		throw new RuntimeException(" This Link is read-only ");  
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public final <T> Optional<T> getAttribute(AKey<T> key) {
		return Optional.ofNullable((T) container.getProperty(key.toString(),null));
	}


}
