package org.pickcellslab.pickcells.neodb.adapters;

import java.util.Iterator;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.pickcellslab.foundationj.datamodel.tools.Path;

public class NeoAdaptedPath implements Path<Node, Relationship> {

	private final org.neo4j.graphdb.Path p;

	public NeoAdaptedPath(org.neo4j.graphdb.Path p) {
		this.p = p;
	}

	@Override
	public Node first() {
		return p.startNode();
	}

	@Override
	public Node last() {
		return p.endNode();
	}

	@Override
	public Relationship firstEdge() {
		Iterator<Relationship> it = p.relationships().iterator();
		if(it.hasNext())
			return it.next();
		return null;
	}

	@Override
	public Relationship lastEdge() {
		return p.lastRelationship();
	}

	@Override
	public Iterator<Node> nodes() {
		return p.nodes().iterator();
	}
		

	@Override
	public Iterator<Relationship> edges() {
		return p.relationships().iterator();
	}

	@Override
	public int nodesCount() {
		return p.length()+1;
	}

	@Override
	public int edgesCount() {
		return p.length();
	}
	
	public String toString(){
		return p.toString();
	}



}
