package org.pickcellslab.pickcells.neodb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.UniqueFactory;
import org.neo4j.graphdb.index.UniqueFactory.UniqueEntity;
import org.neo4j.kernel.DeadlockDetectedException;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.QInterpreter;
import org.pickcellslab.foundationj.dbm.events.DataUpdateListener;
import org.pickcellslab.foundationj.dbm.queries.StorageBox;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StorageBoxInterpreter implements QInterpreter<StorageBox> {

	private final IdGenerator IdGen;
	private final GraphDatabaseService db;

	private Transaction tx = null;

	private static int qCount = 0;
	private int counter = 0;

	//private int statsCount = 0;

	/**
	 * Limit of writes in one transaction
	 */
	private int limit = 20000;
	/**
	 * A flag to tell whether or not to force writing objects with a database id which is unknown to this database.
	 */
	private boolean forceUnknownId = false;


	private static final Logger log = LoggerFactory.getLogger(StorageBoxInterpreter.class);
	static final String idString = DataItem.idKey.toString();

	private final Map<WritableDataItem, Node> sources = new LinkedHashMap<>();
	private final Map<WritableDataItem, Node> targets = new LinkedHashMap<>();
	private List<Link> linksToStore = new ArrayList<>();

	private final Map<String, NodeFactory> nFactories = new HashMap<>();

	private final MetaModelStorer metaStorer;


	//private SummaryStatistics statsLabel = new SummaryStatistics();
	//private SummaryStatistics statsFind = new SummaryStatistics();
	//private SummaryStatistics statsUpdate = new SummaryStatistics();
	//private SummaryStatistics statsCreate = new SummaryStatistics();


	//TODO fireEvents

	StorageBoxInterpreter(MetaModelStorer metaStorer, IdGenerator IdGen) {
		this.IdGen = IdGen;
		this.db = metaStorer.getDB();
		this.metaStorer = metaStorer;
	}





	@Override
	public synchronized void run(StorageBox query) throws DataAccessException  {


		long start = System.currentTimeMillis();
		log.info("Query "+(++qCount)+": "+ query.description());


		//	statsCount = 0;
		//	statsLabel.clear();
		//	statsFind.clear();
		//	statsUpdate.clear();
		//	statsCreate.clear();


		// All the items inside the Storage box must be within the same transaction
		// However in some case, if items are large or there are a lot of items to store
		// OutOfMemoryException will occur
		// -> The StorageBox query holds a limit per commit for this purpose. We need to trust the user's limit

		counter = 0;
		limit = query.transactionSizeLimit();
		forceUnknownId = query.forceUnknownId();

		//Start the transaction
		//-------------------------------------------------------------------------------------------------------

		try {



			tx = db.beginTx();

			log.trace("Starting storage");
			log.trace("Limit of transaction size = "+limit);

			final  List<StepTraverser<NodeItem, Link>> trList = query.getTraversers();
			for (int i = 0; i<trList.size(); i++) {

				final StepTraverser<NodeItem,Link> t = trList.get(i);
				
				log.trace("new traversal step");
				storeTraversal(t);

				log.trace("step stored");

				if(query.hasMetaInfo())
					metaStorer.store(query.getMetaInfoForTraverser(i));

				log.trace("meta stored");
			}


			for(WritableDataItem i : query.getStandAlones()){
				createIfNotExist((NodeItem)i);
			}


			long middle = System.currentTimeMillis();


			log.trace("done with links and nodes storage "+(middle-start)+"ms");


			if(query.hasMetaInfo()){
				log.trace("Starting MetaModel store");
				metaStorer.store(query.getMetaInfoForStandalones());
			}

			//clean before commit
			sources.clear();
			targets.clear();
			linksToStore.clear();
			System.gc();


			tx.success();

		}
		catch(DeadlockDetectedException dde){
			log.error("Transaction failure",dde);
			tx.failure();
			throw new DataAccessException("Dead lock detected during concurrent writes", dde);
		}
		catch(DataAccessException e){
			log.error("Transaction failure",e);
			tx.failure();
			throw e;
		}
		finally{			
			tx.close();
		}



		//Clean up
		sources.clear();
		targets.clear();
		linksToStore.clear();
		nFactories.clear();

		long after = System.currentTimeMillis();
		log.info("Query "+qCount+" took "+(after-start)+"ms");


		//Commit was successful tell listeners in a new thread so this method returns immediately 

		if(query.hasMetaInfo())
			new Thread(new Runnable(){
				@Override
				public void run() {
					metaStorer.validateCommit();
				}      
			}).start();



	}



	/**
	 * This method simply keeps track of the number of items to commit
	 * and trigger the commit and restart a transaction if the limit is reached.
	 */
	private void updateCount() {
		if (counter++ == limit) {
			tx.success(); tx.close();
			tx = db.beginTx();
			counter = 0;
			log.debug("Limit of items in a transaction reached ("+limit+") -> transaction committed and novel started!");
		}	
		/*
		if(++statsCount == 500){
			log.info("Labeling: "+statsLabel.getMean()+" ms");
			log.info("Finding: "+statsFind.getMean()+" ms");
			log.info("Updating: "+statsUpdate.getMean()+" ms");
			log.info("Creating: "+statsCreate.getMean()+" ms");
			statsCount = 0;
			statsLabel.clear();
			statsFind.clear();
			statsUpdate.clear();
			statsCreate.clear();
		}
		 */

	}




	private void storeTraversal(StepTraverser<NodeItem, Link> t) throws DataAccessException{

		log.trace("new graph to store");

		sources.clear();
		targets.clear();
		linksToStore.clear();


		TraversalStep<NodeItem,Link> step = null;

		int itemCounter = 0;
		int linkCounter = 0;



		while ((step = t.nextStep()) != null) {


			log.trace("Step : "+ step.round);

			for(NodeItem i : step.nodes){
				targets.put(i, createIfNotExist(i));
				itemCounter++;
			}
			log.trace("items stored = "+ itemCounter);

			for(Link l : linksToStore){
				createLinkIfNotExist(l, tx);
				linkCounter++;
			}
			log.trace("edges stored = "+ linkCounter);

			sources.putAll(targets);
			targets.clear();
			linksToStore = step.edges;


		}


		for(Link l : linksToStore){
			createLinkIfNotExist(l, tx);
			linkCounter++;
		}


		log.debug("edges stored = "+ linkCounter);


	}



	private Node createIfNotExist(NodeItem item) throws DataAccessException{


		//long bLabel = System.currentTimeMillis();

		//Check class and get adequate label
		NodeFactory uF = nFactories.get(item.declaredType()); 
		if(null == uF){

			//log.trace("Index exist for "+classLabel+" : "+ db.index().existsForNodes(classLabel));
			uF = new NodeFactory(db, item);
			nFactories.put(item.declaredType(), uF);
			IdGen.createIfInexistant(item.declaredType(), tx);

		}

		//long aLabel = System.currentTimeMillis();
		//statsLabel.addValue(aLabel-bLabel);



		Node node = null;

		// If persistedId is null then it was never persisted otherwise recover the
		// node for this item

		Object foundId = item.getAttribute(DataItem.idKey).orElse(null);
		if (foundId != null) {

			//	long bFind = System.currentTimeMillis();

			UniqueEntity<Node> unique = uF.getOrCreateWithOutcome(idString, foundId);
			node = unique.entity();

			if(unique.wasCreated()){

				if(forceUnknownId){// should be very rare

					// Add properties to the newly created node
					final Node fnode = node;
					item.getValidAttributeKeys().forEach(e->fnode.setProperty(e.toString(), item.getAttribute(e).get()));

					//Update IdGen
					IdGen.setProperty(item.declaredType(), foundId);

					//Update count and return the new node
					updateCount();

					return node;

				}else{//If this was forbidden

					node.delete();
					throw new DataAccessException(
							"Inconsistent Id: A node with declaredType : "+item.declaredType()+" and id "+foundId+" is unknown to this database");

				}

			}



			//	long aFind = System.currentTimeMillis();
			//	statsFind.addValue(aFind-bFind);

			//	long bUpdate = System.currentTimeMillis();
			//Update
			final Node fnode = node;
			item.getValidAttributeKeys().forEach(e->fnode.setProperty(e.toString(), item.getAttribute(e).get()));

			//	long aUpdate = System.currentTimeMillis();
			//	statsUpdate.addValue(aUpdate-bUpdate);

		}
		else {

			//	long bCreate = System.currentTimeMillis();


			// Create a new unique id	

			int id = IdGen.newId(uF.label().name(), tx);
			item.setAttribute(WritableDataItem.idKey, id);

			// creating the node
			node = uF.getOrCreate(idString, id);
			final Node fnode = node;
			item.getValidAttributeKeys().forEach(e->fnode.setProperty(e.toString(), item.getAttribute(e).get()));

			//	long aCreate = System.currentTimeMillis();
			//	statsCreate.addValue(aCreate-bCreate);

			// tx.success();
			// }
		}

		updateCount();

		return node;

	}



	private void createLinkIfNotExist(Link l, Transaction tx) throws DataAccessException {

		// If persistedId is null then it was never persisted otherwise recover the
		// relationship for this link
		
		Object foundId = l.getAttribute(WritableDataItem.idKey).orElse(null);
		if (foundId != null) {
			
			Node source = sources.get(l.source());
			for(Relationship r : source.getRelationships(Direction.OUTGOING, RelationshipType.withName(l.declaredType()))) {
				if(r.getProperty(idString).equals(foundId))
					l.getValidAttributeKeys().forEach(k->r.setProperty(k.toString(), l.getAttribute(k).get()));
			}
			log.trace("Relationship keys updated for link "+l);
		}

		else {


			if(l.source() == null)
				throw new DataAccessException("Null Source is illegal; found in link of type "+l.declaredType());
			if(l.target() == null)
				throw new DataAccessException("Null Target is illegal; found in link of type "+l.declaredType());

			// try (Transaction tx = db.beginTx()) {

			String linkType = l.declaredType();

			IdGen.createIfInexistant(linkType, tx);


			// creating the Link
			// get Source node and target node
			Node source = sources.get(l.source());
			Node target = targets.get(l.target());

			// Traverser returns both incoming and outgoing links
			// Plus there may be self-referencing links

			if(source == null)
				source = targets.get(l.source());
			if(target == null)
				target = sources.get(l.target());


			//This null check is necessary when a partial graph is stored (Using TraverserConstraint)
			//Links are returned during one round but the adjacent will not be included
			//So we don't include these links in the database.
			if(source!= null && target != null){
				Relationship r = source.createRelationshipTo(target,
						RelationshipType.withName(l.declaredType()));


				l.getValidAttributeKeys().forEach(k->r.setProperty(k.toString(), l.getAttribute(k).get()));
				
				// get the id and add to both node and item instance

				int id = IdGen.newId(linkType, tx);
				l.setAttribute(WritableDataItem.idKey, id);
				r.setProperty(WritableDataItem.idKey.toString(), id);


				// tx.success();
				// }
			}
		}

		updateCount();
	}




	@Override
	public void setUpdateListenerList(List<DataUpdateListener> lstrs) {
		//TODO this.lstrs = lstrs;
	}


	private class NodeFactory extends UniqueFactory.UniqueNodeFactory{

		private final Consumer<Node> labeler;
		private final Label declaredTypeLabel;
		private final String typeIdValue;
		private final Set<Label> tags;


		NodeFactory( GraphDatabaseService db, NodeItem item){
			super(db,item.declaredType());
			typeIdValue = item.typeId();
			declaredTypeLabel = Label.label(item.declaredType());
			
			//System.out.println("New factory for typeId : "+typeIdValue);
			//System.out.println("New factory for typeId : "+declaredTypeLabel);
			
			tags = Helpers.getTags(item.getClass()).keySet();
			if(item.declaredType().equals(item.typeId())){
				labeler = n -> n.addLabel(declaredTypeLabel);
			}
			else{
				final Label typeIdLabel = Label.label(typeIdValue);
				labeler = n -> {
					n.addLabel(declaredTypeLabel);
					n.addLabel(typeIdLabel);
				};
			}
		};		


		Label label(){
			return declaredTypeLabel;
		}

		Set<Label> tags(){
			return tags;
		}


		@Override
		protected void initialize( Node created, Map<String, Object> properties )
		{
			created.setProperty( idString, properties.get( idString ) );
			created.setProperty(Helpers.typeIdKey, typeIdValue);
			
			labeler.accept(created);
			for(Label l : tags()){
				created.addLabel(l);							
			}
			
			//Now add the typeId the new node
			//System.out.println("Type Id = "+typeIdValue);
			
		}

	}




}
