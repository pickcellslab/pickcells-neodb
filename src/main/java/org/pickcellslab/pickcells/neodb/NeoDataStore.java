package org.pickcellslab.pickcells.neodb;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.graphdb.index.Index;
import org.neo4j.io.fs.FileUtils;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.DataStoreManager;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.QInterpreter;
import org.pickcellslab.foundationj.dbm.events.ConnexionFailedException;
import org.pickcellslab.foundationj.dbm.events.DataUpdateListener;
import org.pickcellslab.foundationj.dbm.events.MetaListenerNotifier;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.StorageBox;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.neodb.helpers.Helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NeoDataStore implements DataStoreManager<Node,Relationship> {


	private static final Logger log = LoggerFactory.getLogger(NeoDataStore.class);
	private GraphDatabaseService db;
	private final IdGenerator IdGen;
	private boolean isClosed = false;
	private final String dbPath;
	private final DataRegistry registry;

	NeoDataStore(ConnexionRequest request, DataRegistry registry) throws DataAccessException, ConnexionFailedException {
		
		dbPath = request.dbPath()+File.separator+request.dbName();
		
		// Connect to the database
		db = new GraphDatabaseFactory()
				.newEmbeddedDatabaseBuilder( new File(dbPath) )
				.setConfig( GraphDatabaseSettings.allow_upgrade, "true" )
				.newGraphDatabase();

		registerShutdownHook(db);

		this.registry = registry;

		// Is this request allowed?
		
		
		// If not throw Excepion
		// TODO throw new ConnexionFailedException("Permission Denied");
		
		IdGen = new IdGenerator(db);

		updateIfRequired(db);

		
		log.debug("New NeoDatastore created");
	}


	
	
	
	
	
	
	
	
	@Override
	public DatabaseAccess<Node,Relationship> newAccess() {
		if(isClosed)
			throw new RuntimeException("The database is closed");
		return new NeoDataAccess(db, registry);
	}


	@Override
	public void addUpdateListener(DataUpdateListener lstr) {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean removeUpdateListener(DataUpdateListener lstr) {
		// TODO Auto-generated method stub
		return false;
	}






	@Override
	public void createSavePoint(String name) {
		// use backup system TODO
		
	}

	@Override
	public void revertToSavePoint(String name) {
		// use backup system TODO

	}


	@Override
	public void copyDataBase(String path) throws DataAccessException {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void deleteDataBase() {
		close();
		try {
			FileUtils.deleteRecursively(new File(dbPath));
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
		db = null;
	}





	@Override
	public void close() {
		if(null != db)
			db.shutdown();
		db = null;
		isClosed = true;
	}


	@Override
	public boolean isClosed() {
		return isClosed ;
	}


	@Override
	public QInterpreter<MetaBox> createMetaStorer(MetaListenerNotifier notifier) {
		return new MetaModelStorer(db,notifier);
	}


	@Override
	public QInterpreter<StorageBox> createDataStorer(MetaListenerNotifier notifier) {
		return new StorageBoxInterpreter(new MetaModelStorer(db,notifier), IdGen);
	}


	private void updateIfRequired(GraphDatabaseService db) throws DataAccessException{


		final int expectedVersion = 3;
		int currentVersion = IdGen.dbVersion(db);
		System.out.println("DataStore Current Version = "+currentVersion);

		while(currentVersion!=expectedVersion){
			switch(currentVersion){
			case -1: IdGen.updateVersion(db, expectedVersion); break; // db just created
			case 0 : JOptionPane.showMessageDialog(null, "This database version is too old and cannot be updated.");
			throw new DataAccessException("Unrecognised database version "+expectedVersion);//perform0to1Upgrade(db); break;
			case 1 : perform1to3Upgrade(db); break;
			case 2: perform2to3Upgrade(db); break;
			case 3: return;
			default: throw new DataAccessException("Unrecognised database version "+expectedVersion);
			}
			currentVersion = IdGen.dbVersion(db);
		}

	}


	private void perform1to3Upgrade(GraphDatabaseService db2) throws DataAccessException {


		//========= Warn the user ==============

		int choice = JOptionPane.showConfirmDialog(null, "A Database update is required (from v1 to v2)!"
				+ "\nData will be conserved, however, data annotations such as filters or custom attributes will be lost in the process.\n"
				+ "The database will also be incompatble with previous versions of PickCells.\n"
				+ "On the bright side you get fewer bugs and a more stable PickCells ;) !\n\n"
				+ "Do you wish to continue?", "Database Update...", JOptionPane.YES_NO_OPTION);

		if(choice == JOptionPane.NO_OPTION)
			throw new DataAccessException("Unable to update the data store");



		final Progress progress = new Progress(); 
		progress.createAndShowGUI();
		progress.refresh();

		Transaction tx = db.beginTx();


		//========= Replace all properties in the database ==============


		System.out.println("Node Properties update...");

		//Nodes
		db.getAllNodes().forEach(n->{

			final Map<String, Object> properties = n.getAllProperties();
			properties.forEach((p,o)->{
				if(p.contains(":")){
					n.removeProperty(p);
					n.setProperty(AKey.get(p).toString(), o);
				}
			});

		});



		progress.setTaskDone(30, "Node Properties update");

		//Relationships

		System.out.println("Link Properties update...");
		db.getAllRelationships().forEach(n->{

			final Map<String, Object> properties = n.getAllProperties();
			properties.forEach((p,o)->{
				if(p.contains(":")){
					n.removeProperty(p);
					n.setProperty(AKey.get(p).toString(), o);
				}
			});

		});

		progress.setTaskDone(60, "Link Properties update");

		final Map<String,String> cache = new HashMap<>();

		// ============= Replace all labels in the database and add the typeid key ==================



		db.getAllNodes().forEach(n->{
			//find the class label
			Label l = null;
			for(Label label : n.getLabels()){
				if(label.name().endsWith("Class")){
					l = label;					
					break;
				}
			}

			if(l == null)
				return;			

			//delete the label
			if(!l.name().equals("MetaClass")){



				n.removeLabel(l);

				//Identify SimpleName
				String id = cache.get(l.name());
				if(id==null){
					final String noPackage = l.name().substring(l.name().lastIndexOf(".")+1);
					//if(!noPackage.equals("MetaClass"))
					id = noPackage.substring(0, noPackage.length()-5);
					cache.put(l.name(), id);
				}

				//Add as typeId property
				n.setProperty(Helpers.typeIdKey, id);
				n.addLabel(Label.label(id));

				if(id.equals("MetaClass")){// MetaClass particular case
					String toReplace = (String) n.getProperty("Class:String");
					n.setProperty("Class:String", toReplace.substring(toReplace.lastIndexOf(".")+1));
					//System.out.println("MetaClass typeId replaced!");
				}
				else if(id.equals("MetaKey")){// MetaKey particular case
					String toReplace = (String) n.getProperty(MetaModelStorer.recString);
					String name = (String) n.getProperty(MetaModel.name.toString());
					String type = (String) n.getProperty("ObjectType:String");
					String oldKey = name+":"+type;
					String target =toReplace.substring(oldKey.length());
					AKey<?> k = AKey.get(oldKey);
					n.setProperty("ObjectType:String", k.clazz);
					n.setProperty(MetaModelStorer.recString, k.toString()+target);
					//System.out.println("MetaClass typeId replaced!");
				}

			}

		});


		tx.success();
		tx.close();

		tx = db.beginTx();

		//========= Remove index on old Database id strings and create a new ones  ==============

		cache.forEach((old,novel)->{
			//remove index
			db.index().forNodes(old).delete();

		});

		tx.success();
		tx.close();







		//Create indices

		tx = db.beginTx();

		cache.forEach((old,novel)->{
			//Add node to indices		
			if(!novel.startsWith("Meta")){
				final Index<Node> index =  db.index().forNodes(novel);
				db.findNodes(Label.label(novel)).forEachRemaining(n->{
					index.add(n, StorageBoxInterpreter.idString, n.getProperty(StorageBoxInterpreter.idString));
				});;
			}

		});



		final String metaModel = "MetaModel";
		final Index<Node> index =  db.index().forNodes(metaModel);
		db.findNodes(Label.label(metaModel)).forEachRemaining(n->{
			index.add(n, MetaModelStorer.recString, n.getProperty(MetaModelStorer.recString));
		});


		tx.success();
		tx.close();




		progress.setTaskDone(90, "Node Labels update");

		tx = db.beginTx();

		// =================== Replace the IdGen properties ======================

		final Map<String, Object> properties = IdGen.getAllProperties();
		properties.forEach((p,o)->{
			if(p.endsWith("Class") && cache.get(p)!=null){					
				IdGen.setProperty(cache.get(p), o);
				IdGen.removeProperty(p);
			}
		});



		//Remove broken: MetaFunction, MetaFilter, MetaTraversal... 

		db.findNodes(Label.label("MetaTraversal")).forEachRemaining(n->{
			n.getRelationships().forEach(l->l.delete());
			n.delete();
		});

		db.findNodes(Label.label("MetaFilter")).forEachRemaining(n->{
			n.getRelationships().forEach(l->l.delete());
			n.delete();
		});

		db.findNodes(Label.label("MetaFunction")).forEachRemaining(n->{
			n.getRelationships().forEach(l->l.delete());
			n.delete();
		});

		db.findNodes(Label.label("MetaClass")).forEachRemaining(n->{
			if(n.getProperty(MetaModel.name.toString()).equals("MetaClass")){
				n.getRelationships().forEach(l->l.delete());
				n.delete();
			}
		});


		IdGen.updateVersion(db, 3);

		tx.success();
		tx.close();



		progress.setTaskDone(100, "Done..");
		progress.dispose();

	}


	private void perform2to3Upgrade(GraphDatabaseService db) throws DataAccessException {

		// Repare MetaModel Index


		//========= Remove old MetaModel index  ==============

		Transaction tx = db.beginTx();		

		db.index().forNodes("MetaModel").delete();

		tx.success();
		tx.close();


		//======== Now rebuild the index ===============

		tx = db.beginTx();

		final Index<Node> index = db.index().forNodes("MetaModel");
		db.findNodes(Label.label("MetaModel")).forEachRemaining(n->{
			index.add(n, MetaModelStorer.recString, n.getProperty(MetaModelStorer.recString));
		});;

		IdGen.updateVersion(db, 3);

		tx.success();
		tx.close();



	}


	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				log.debug("Shutingdown database from the hook");
				graphDb.shutdown();
			}
		});
	}





}
