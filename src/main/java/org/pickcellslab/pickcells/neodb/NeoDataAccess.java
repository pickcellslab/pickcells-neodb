package org.pickcellslab.pickcells.neodb;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.FilteredIterator;
import org.pickcellslab.foundationj.datamodel.tools.PathTraverser;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.db.MarkedState;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.neodb.adapters.AdaptedTraverser;
import org.pickcellslab.pickcells.neodb.adapters.NeoItemFactory;
import org.pickcellslab.pickcells.neodb.adapters.Reconstructor;

public class NeoDataAccess implements DatabaseAccess<Node,Relationship> {



	private final GraphDatabaseService db;
	private final Transaction tx;
	private final NeoItemFactory factory;
	private final DataRegistry registry;

	NeoDataAccess(GraphDatabaseService db, DataRegistry registry){
		this.db = db;
		this.registry = registry;
		this.factory = new NeoItemFactory(db);
		tx = db.beginTx();
	}


	@Override
	public Iterator<Node> getNodes(Class<? extends NodeItem> clazz) {
		return db.findNodes(Label.label(DataRegistry.typeIdFor(clazz)));
	}

	@Override
	public <T> Optional<Node> getOne(Class<? extends NodeItem> clazz, AKey<T> k, T value) {
		Optional<Node> opt = null;
		ResourceIterator<Node> it = db.findNodes(Label.label(DataRegistry.typeIdFor(clazz)), k.toString(), value);
		if(it.hasNext())
			opt = Optional.of(it.next());
		else 
			opt = Optional.empty();
		it.close();
		return opt;
	}
	
	@Override
	public <T> Optional<NodeItem> getOneReadOnly(Class<? extends NodeItem> clazz, AKey<T> k, T value) {
		return this.getOneReadOnly(DataRegistry.typeIdFor(clazz), k, value);
	}
	
	
	@Override
	public <T> Optional<NodeItem> getOneReadOnly(String label, AKey<T> k, T value) {
		Optional<NodeItem> opt = null;
		ResourceIterator<Node> it = db.findNodes(Label.label(label), k.toString(), value);
		if(it.hasNext())
			opt = Optional.of(factory.getNodeReadOnly(it.next()));
		else 
			opt = Optional.empty();
		it.close();
		return opt;
	}
	

	@Override
	public Iterator<Node> getNodes(String label) {
		return db.findNodes(Label.label(label));
	}

	@Override
	public <T> Optional<Node> getOne(String label, AKey<T> k, T value) {
		Optional<Node> opt = null;
		ResourceIterator<Node> it = db.findNodes(Label.label(label), k.toString(), value);
		if(it.hasNext())
			opt = Optional.of(it.next());
		else 
			opt = Optional.empty();
		it.close();
		return opt;
	}

	@Override
	public Iterator<Relationship> getLinks(String type) {
		return new FilteredIterator<>(
				db.getAllRelationships().iterator(),
				(r)->r.isType(RelationshipType.withName(type)));
	}
	
	
	
	@Override
	public Iterator<Relationship> getLinks(String type, String srcType, String tgType) {
			
		final RelationshipType linkType = RelationshipType.withName(type);
		final Label srcLabel = Label.label(srcType);
		final Label tgLabel = Label.label(tgType);
		
		return new FilteredIterator<>(
				db.getAllRelationships().iterator(),
				(r)->r.isType(linkType) && r.getStartNode().hasLabel(srcLabel) && r.getEndNode().hasLabel(tgLabel));
	}
	
	
	

	@Override
	public <T> Optional<Relationship> getOneLink(String linkType, AKey<T> k, T value) {
		Relationship r = null;
		final Iterator<Relationship> it = getLinks(linkType);
		final String p = k.toString();
		while(it.hasNext()){
			r = it.next();
			if(r.getProperty(p).equals(value))
				break;
		}
		return Optional.ofNullable(r);
	}

	@Override
	public PathTraverser<Node, Relationship> traverser(TraversalDefinition td) {
		return new AdaptedTraverser(db,td, factory);
	}

	@Override
	public FjAdaptersFactory<Node, Relationship> createFactory() {
		return factory;
	}
	
	
	@Override
	public void success() {
		tx.success();
	}

	@Override
	public void fail() {
		tx.failure();
	}


	@Override
	public void close() throws Exception {
		tx.close();
	}


	@Override
	public RegeneratedItems regenerate(List<Node> nodes, ReconstructionRule rule, boolean delete) throws DataAccessException {
		return new Reconstructor(db, factory, registry).retrieve(nodes, rule, delete);
	}


	@Override
	public MarkedState newThread() {
		return new NeoMarkedState(db.beginTx());
	}


	@Override
	public <T> Optional<Link> getOneLinkReadOnly(String linkType, AKey<T> k, T value ) {
		Relationship r = null;
		final Iterator<Relationship> it = getLinks(linkType);
		final String p = k.toString();
		while(it.hasNext()){
			r = it.next();
			if(r.getProperty(p).equals(value))
				break;
		}
		return Optional.ofNullable(factory.getLinkReadOnly(r));
	}



	


}
