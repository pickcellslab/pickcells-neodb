package org.pickcellslab.pickcells.neodb;

import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.pickcells.neodb.adapters.NeoItemFactory;

public class AdaptedLinkEvaluator implements Evaluator {

	private final ExplicitPredicate<? super Link> filter;
	private final Evaluation ifYes;
	private final NeoItemFactory f;
	private final Evaluation ifNot;
	private RelationshipType t;
	private Evaluation dflt;

	public AdaptedLinkEvaluator(RelationshipType t, ExplicitPredicate<? super Link> filter, Evaluation ifYes, Evaluation ifNot, Evaluation dflt, NeoItemFactory f) {
		this.filter = filter;
		this.f = f;
		this.t = t;
		this.dflt = dflt;
		this.ifYes = ifYes;
		this.ifNot = ifNot;
	}

	@Override
	public Evaluation evaluate(final Path path) {
		if(path.lastRelationship()==null)
			return dflt;
		if(!path.lastRelationship().isType(t))
			return dflt;
		Link l = f.getLinkReadOnly(path.lastRelationship());
		if(filter.test(l))
			return ifYes;
		else return ifNot;
	}


}
