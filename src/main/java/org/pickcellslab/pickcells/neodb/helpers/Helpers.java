package org.pickcellslab.pickcells.neodb.helpers;

import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.neo4j.graphdb.ConstraintViolationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.traversal.Evaluation;
import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.MorphedIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Guillaume
 *
 */
public abstract class Helpers {

	private static Logger log = LoggerFactory.getLogger(Helpers.class);	

	public static final String typeIdKey = "typeId";


	/**
	 * ONLY FOR Old DATABASE VERSIONS UPDATE
	 * Stores the convention for creating a label name for a specific class of DataItem
	 * @return the string built using this convention
	 */
	@Deprecated
	public static String makeLabel(Class<? extends WritableDataItem> clazz){
		return clazz.getName()+"Class";
	}




	public static Node createUniqueNode(String label, String uniqueProperty, GraphDatabaseService db) {

		Node node = db.createNode();
		// Add the label
		Label l = Label.label(label);
		node.addLabel(l);
		try {
			db.schema().indexFor(l).on(uniqueProperty).create();
			db.schema().constraintFor(l).assertPropertyIsUnique(uniqueProperty).create();
		}
		catch (ConstraintViolationException e) {
			System.out.println(e.getMessage());
			// ignore as this means the index already exists and was not added;
		}

		return node;
	}

	public static boolean labelExists(String name, GraphDatabaseService db) {
		boolean labelExists = false;
		ResourceIterable<Label> labels = db.getAllLabels();
		for (Label l : labels) {
			if (l.name().equals(name)) {
				labelExists = true;
				break;
			}
		}
		return labelExists;
	}






	/**
	 * This method identifies the interface and superclass of the provided class which are annotated
	 * with {@link Tag}. It then collects the encountered tags and maps the generated neo4j label to
	 * a String[2] where index 0 is the creator and index[1] is the description
	 * @param clazz
	 * @param label
	 * @param map
	 */
	public static Map<Label, String[]> getTags(Class<? extends DataItem> clazz) {

		final Map<Label, String[]> tagSet= new HashMap<>();

		// Queue to go up the tree if necessary
		final Deque<Class<?>> queue = new LinkedList<>();
		queue.add(clazz);

		do {
			Class<?> mayHaveTag = queue.poll(); // this can be either class or interface

			// if this is not an interface it may be annotated with @Data
			if(!mayHaveTag.isInterface()) {
				final Tag t = mayHaveTag.getAnnotation(Tag.class);
				if(t!=null)
					tagSet.put(Label.label(t.tag()), new String[]{t.creator(),t.description()}); 
				// Add the superclass to the queue if it exists
				final Class<?> superClass = mayHaveTag.getSuperclass();
				if(superClass!=null)
					queue.push(superClass);
			}

			//Check directly implemented interfaces
			for(Class<?> itrfc : mayHaveTag.getInterfaces()){				
				final Tag t = itrfc.getAnnotation(Tag.class);
				if(t!=null) {
					if(t!=null)
						tagSet.put(Label.label(t.tag()), new String[]{t.creator(),t.description()});
				}
				queue.push(itrfc);
			}

		}
		while(!queue.isEmpty());

		return tagSet;	
		
	}





	public static Iterator<String> getTags(Node n){
		return new MorphedIterator<Label,String>(n.getLabels().iterator(),(l)->l.name());
	}




	/**
	 * @param itr
	 * @return An {@link Iterable} which can only be used once
	 */
	public  static <T> Iterable<T> asIterable(ResourceIterator<T> itr){
		return new Iterable<T>(){
			@Override
			public Iterator<T> iterator() {
				return itr;
			}
		};
	}




	/**
	 * Converts a foundationJ Direction into a Neo4j Direction
	 * @param d
	 * @return
	 */
	public static org.neo4j.graphdb.Direction getNeo4jDirection(Direction d){

		org.neo4j.graphdb.Direction neoD = null;

		switch(d){
		case BOTH: neoD = org.neo4j.graphdb.Direction.BOTH;
		break;
		case INCOMING: neoD = org.neo4j.graphdb.Direction.INCOMING;
		break;
		case OUTGOING: neoD = org.neo4j.graphdb.Direction.OUTGOING;
		break;
		default:
			break;		
		}		

		return neoD;

	}





	public static Evaluation getNeo4jDecision(Decision value) {
		Evaluation eval = null;
		switch(value){
		case EXCLUDE_AND_CONTINUE: eval = Evaluation.EXCLUDE_AND_CONTINUE;
		break;
		case EXCLUDE_AND_STOP: eval = Evaluation.EXCLUDE_AND_PRUNE;
		break;
		case INCLUDE_AND_CONTINUE: eval = Evaluation.INCLUDE_AND_CONTINUE;
		break;
		case INCLUDE_AND_STOP: eval = Evaluation.INCLUDE_AND_PRUNE;
		break;
		default: eval = null;
		break;			
		}
		return eval;
	}



}
