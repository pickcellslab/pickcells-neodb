package org.pickcellslab.pickcells.neodb;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.DataStoreConnector;
import org.pickcellslab.foundationj.dbm.db.DataStoreManager;
import org.pickcellslab.foundationj.dbm.events.ConnexionFailedException;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CoreImpl
public class NeoAdmin implements DataStoreConnector<Node,Relationship> {

	private static final Logger log = LoggerFactory.getLogger(NeoAdmin.class);

	private final Map<String, NeoDataStore> active = new HashMap<>();
	
	
	@Override
	public DataStoreManager<Node, Relationship> connect(ConnexionRequest request, DataRegistry registry) throws ConnexionFailedException{
		synchronized(active){
			NeoDataStore db = active.get(request.dbPath());			
			if(db == null) {
				try {
					db = new NeoDataStore(request, registry);
					active.put(request.dbPath(), db);					
				}catch(DataAccessException e) {
					log.debug("The connection to "+request.dbPath()+" failed - user:" + request.userName());
					throw new ConnexionFailedException("The connection to "+request.dbPath()+" failed.", e);
				}
			}
			return db;
		}

	}


	@Override
	public boolean isValidDatabase(File file) {
		// Just check if the file neostore.id exists in the given path
		if(file==null)
			return false;
		if(!file.exists())
			return false;
		if(!file.isDirectory())
			return false;
		final String neostore = "neostore";
		for(String content : file.list()) {
			if(content.startsWith(neostore))
				return true;
		}
		return false;
	}


	

}

