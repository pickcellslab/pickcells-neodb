package org.pickcellslab.pickcells.neodb;


import java.awt.BorderLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


class Progress extends JPanel {

	private JProgressBar progressBar;
	private JTextArea taskOutput;
	private JFrame frame;




	public Progress() {
		super(new BorderLayout());


		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);

		//Call setStringPainted now so that the progress bar height
		//stays the same whether or not the string is shown.
		progressBar.setStringPainted(true); 

		taskOutput = new JTextArea(5, 20);
		taskOutput.setMargin(new Insets(5,5,5,5));
		taskOutput.setEditable(false);


		add(progressBar, BorderLayout.PAGE_START);
		add(new JScrollPane(taskOutput), BorderLayout.CENTER);
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	}



	public void setTaskDone(int progress, String task){
		progressBar.setValue(progress);
		taskOutput.append(task + " Done! \n");
		refresh();
	}


	public void refresh(){
		if(frame!=null)
			frame.repaint();
	}

	public void dispose(){
		frame.dispose();
		frame = null;
	}

	/**
	 * Create the GUI and show it. As with all GUI code, this must run
	 * on the event-dispatching thread.
	 * @return 
	 */
	public Progress createAndShowGUI() {
		//Create and set up the window.
		if(frame!=null)
			return this;

		frame = new JFrame("DataBase Update...");

		//Create and set up the content pane.
		this.setOpaque(true); //content panes must be opaque
		frame.setContentPane(this);

		//Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		return this;
	}



}



