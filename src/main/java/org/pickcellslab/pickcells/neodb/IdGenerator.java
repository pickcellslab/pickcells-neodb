package org.pickcellslab.pickcells.neodb;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

/**
 * 
 * The Node responsible for keeping track of 
 * the last id used for each type of DataItem
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("deprecation")
class IdGenerator implements Node {

	private boolean firstLogin;
	private final Node delegate;

	IdGenerator(GraphDatabaseService db){

		Objects.requireNonNull(db);

		try (Transaction tx = db.beginTx()) {

			ResourceIterator<Node> i = db.findNodes(
					DynamicLabel.label("IdGenerator"));
			if (!i.hasNext()) {
				delegate = db.createNode();
				delegate.addLabel(DynamicLabel.label("IdGenerator"));
				delegate.setProperty("Created on", new Date().toString());
				firstLogin = true;
			}
			else{
				delegate = i.next();
				firstLogin = false;
			}
			
			
			tx.success();
		}
	}

	
	int dbVersion(GraphDatabaseService db){
		
		if(firstLogin){
			return -1;
		}
		
		int ur;
		try (Transaction tx = db.beginTx()) {
			ur = (int)delegate.getProperty("Version", 0);
			tx.success();
		}
		return ur;
	}
	
	void updateVersion(GraphDatabaseService db, int version){
		try (Transaction tx = db.beginTx()) {
			delegate.setProperty("Version", version);
			firstLogin = false;
			tx.success();
		}
	}
	
	/**
	 * Creates a new entry for the given label if it does not yet exist
	 * @param label
	 */
	public synchronized void createIfInexistant(String label, Transaction tx){
		//lock.writeLock().lock();
		//Lock l = tx.acquireWriteLock(delegate);

		if(!delegate.hasProperty(label))
			delegate.setProperty(label, -1);

		//l.release();

		//lock.writeLock().unlock();
	}

	public int newId(String name, Transaction tx){
		Integer id = (Integer)delegate.getProperty(name, 0) + 1;
		delegate.setProperty(name, id);
		return id;
	}

	@Override
	public GraphDatabaseService getGraphDatabase() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasProperty(String key) {
		return delegate.hasProperty(key);
	}

	@Override
	public Object getProperty(String key) {
		return delegate.getProperty(key);
	}

	@Override
	public  Object getProperty(String key, Object defaultValue) {
		return delegate.getProperty(key,defaultValue);
	}

	@Override
	public void setProperty(String key, Object value) {
		delegate.setProperty(key, value);
	}

	@Override
	public Object removeProperty(String key) {
		return delegate.removeProperty(key);
	}

	@Override
	public Iterable<String> getPropertyKeys() {
		return delegate.getPropertyKeys();
	}

	@Override
	public Map<String, Object> getProperties(String... keys) {
		return delegate.getProperties(keys);
	}

	@Override
	public Map<String, Object> getAllProperties() {
		return delegate.getAllProperties();
	}

	@Override
	public long getId() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public void delete() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<Relationship> getRelationships() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasRelationship() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<Relationship> getRelationships(RelationshipType... types) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<Relationship> getRelationships(Direction direction, RelationshipType... types) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasRelationship(RelationshipType... types) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasRelationship(Direction direction, RelationshipType... types) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<Relationship> getRelationships(Direction dir) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasRelationship(Direction dir) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<Relationship> getRelationships(RelationshipType type, Direction dir) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasRelationship(RelationshipType type, Direction dir) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Relationship getSingleRelationship(RelationshipType type, Direction dir) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Relationship createRelationshipTo(Node otherNode, RelationshipType type) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<RelationshipType> getRelationshipTypes() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public int getDegree() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public int getDegree(RelationshipType type) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public int getDegree(Direction direction) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public int getDegree(RelationshipType type, Direction direction) {
		throw new RuntimeException("Not Implemented");
	}



	@Override
	public void addLabel(Label label) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public void removeLabel(Label label) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public boolean hasLabel(Label label) {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public Iterable<Label> getLabels() {
		throw new RuntimeException("Not Implemented");
	}


}
