package org.pickcellslab.pickcells.neodb;

import org.neo4j.graphdb.Transaction;
import org.pickcellslab.foundationj.dbm.db.MarkedState;

public class NeoMarkedState implements MarkedState {

	
	private final Transaction tx;

	NeoMarkedState(Transaction tx){
		this.tx = tx;
	}
	
	
	@Override
	public void close() throws Exception {
		tx.close();
	}

	@Override
	public void success() {
		tx.success();
	}

	@Override
	public void fail() {
		tx.failure();
	}

}
