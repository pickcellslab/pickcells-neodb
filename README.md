# README #

This project contains an implementation of [foundationj-dbm](https://framagit.org/pickcellslab/foundationj/tree/master/foundationj-dbm) allowing applications leveraging [foundationj](https://framagit.org/pickcellslab/foundationj) to rely on [Neo4j](http://neo4j.com/) for data management.

The current implementation creates [embedded Neo4j instance](https://neo4j.com/docs/java-reference/current/tutorials-java-embedded/)

